﻿using UnityEngine;
using System.Collections;

public class FormationInvader : MonoBehaviour {

	FormationController controller;

	void Start() {
		controller = transform.parent.gameObject.GetComponent<FormationController>();
	}

	// Update is called once per frame
	void Update () {
		float x = transform.position.x;

		if (x < controller.leftEdge) {
			controller.MoveRight();

		} else if (x > controller.rightEdge) {
			controller.MoveLeft();
		}
	}


}
