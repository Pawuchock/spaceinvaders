﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour {

	public float speed = 3.0f;
	public int pointValue = 10;
	// Update is called once per frame
	void FixedUpdate () {
		rigidbody.velocity = new Vector2(0.0f, speed);

	}
	void OnTriggerEnter(Collider coll)
		{ 
		if (coll.gameObject.name == "Eater") 
				{
						Destroy (gameObject);	
				} 
		if (coll.gameObject.name == "Player") 
				{
						Destroy (gameObject);
						PlayerMovementNshooting.Health -= 50;
				} 
		if (coll.gameObject.name == "Invader")  	
				{
			Destroy (gameObject);
			Destroy (coll.gameObject);
			Stats.currentScore += pointValue;
				}
			
		}
}
