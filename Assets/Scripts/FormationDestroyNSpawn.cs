﻿using UnityEngine;
using System.Collections;

public class FormationDestroyNSpawn : MonoBehaviour {

	public int invadersRemaining;
	public int lolaX,lolaY;
	public GameObject enemyToSpawn;
	// Use this for initialization

	void Update () {
		lolaX = Random.Range(-2, 2);
		lolaY = Random.Range (1, 2);
		invadersRemaining = GameObject.FindGameObjectsWithTag ("Alien").Length;
		if (invadersRemaining == 0) {
			Destroy(GameObject.Find("Aliens(Clone)"));
			Instantiate(enemyToSpawn);
		}
	}	
	

}
