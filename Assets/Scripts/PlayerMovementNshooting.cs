﻿using UnityEngine;
using System.Collections;

public class PlayerMovementNshooting : MonoBehaviour {

	public float speed = 10.0f;
	public float minX = -2.0f;
	public float maxX = 2.0f;
	public GameObject bullet;
	public static int Health ;
	// Update is called once per frame

	void Update () 
	{
		if (Health < 0)
		{
			Application.Quit();
		}
		float delta = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
		
		transform.position += new Vector3(delta, 0.0f);
		
		if (transform.position.x < minX) {
			transform.position = new Vector3(minX, transform.position.y, transform.position.z);
		} else if (transform.position.x > maxX) {
			transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
		}
		if (Input.GetKeyUp("space")) {
			Instantiate(bullet,transform.position, Quaternion.identity);
		

		}
		 
}

		


}