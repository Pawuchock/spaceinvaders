﻿using UnityEngine;
using System.Collections;

public class FormationController : MonoBehaviour {

	public float leftEdge = -5.5f;
	public float rightEdge = 5.5f;

	public float speedX = 0.3f;
	public float speedY = 0.3f;

	bool directionLeft = false;
	bool moveDown = false;

	// Use this for initialization

	
	// Update is called once per frame
	void Update () {
		float deltaY = 0.0f;
		float deltaX = 0.0f;


			// move down
			if (moveDown) {
				moveDown = false;
				deltaY = -speedY;
			}
		//move left
			if (directionLeft) {
				deltaX = -speedX;
			} else {
				deltaX = speedX;
			}
		

		transform.position += new Vector3(deltaX, deltaY);	
	}
	
	public void MoveLeft () {
		directionLeft = true;
		moveDown = true;
	}

	public void MoveRight() {
		directionLeft = false;
		moveDown = true;
	}

}
